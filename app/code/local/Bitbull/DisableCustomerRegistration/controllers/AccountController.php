<?php
/**
 * @category Bitbull
 * @package  Bitbull_DisableCustomerRegistration
 * @author   Nadia Sala <nadia.sala@bitbull.it>
 */
require_once 'Mage/Customer/controllers/AccountController.php';

class Bitbull_DisableCustomerRegistration_AccountController extends Mage_Customer_AccountController {

    /**
     * Customer register form page
     */
    public function createAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*');
            return;
        }

        $this->_redirect('home');
        return;
    }

    /**
     * Create customer account action
     */
    public function createPostAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*');
            return;
        }

        $this->_redirect('home');
        return;
    }

}